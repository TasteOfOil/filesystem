const fs = require('fs');
var fileName = "./FileSystem.txt";
console.log("Create file");
fs.writeFileSync(fileName, "It's new File.", (err)=>{
    err?console.log(err):null;
});
console.log("Read file");
var _data = fs.readFileSync(fileName,'utf-8',(err,data)=>{    
    if(err){
        console.log(err);
    }
});
console.log(_data);
console.log("Update file");
fs.writeFileSync(fileName, `${_data} New text in file`,(err)=>{
    err?console.log(err):null;
});
console.log("Read file");
_data = fs.readFileSync(fileName,'utf-8',(err,data)=>{
    if(err){
        console.log(err);
    }
});
console.log(_data);
console.log("Delete file");
setTimeout(()=>{fs.unlinkSync(fileName, (err)=>{
    err?console.log(err):null;
}); console.log("File deleted");},10000);